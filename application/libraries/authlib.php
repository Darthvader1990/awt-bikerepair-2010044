<?php

class Authlib {

    function __construct() {
	// get a reference to the CI super-object, so we can
	// access models etc. (because we don't extend a core
	// CI class)
	$this->ci = &get_instance();

	$this->ci->load->model('user_model');
    }

    public function register($name, $email, $user, $pwd, $conf_pwd,$userType) {
	if ($name == '' || $user == '' || $email == '' || $pwd == '' || $conf_pwd == '') {
	    return 'Missing field';
	}
	if ($pwd != $conf_pwd) {
	    return "Passwords do not match";
	}
	return $this->ci->user_model->register($name, $email, $user, $pwd, $userType);
    }

    public function login($user, $pwd) {
	if ($user == '' || $pwd == '') {
	    return false;
	}
	return $this->ci->user_model->login($user, $pwd);
    }

    function is_loggedin() {
	return $this->ci->user_model->is_loggedin();
    }
    
    public function changePassword($paUsername, $paOldPass, $paNewPassword) {
	if ($paUsername == '' || $paOldPass == '' || $paNewPassword == '') {
	    return 'Missing field';
	}
	
	return $this->ci->user_model->changePassword($paUsername, $paOldPass, $paNewPassword);
	
    }
}

?>