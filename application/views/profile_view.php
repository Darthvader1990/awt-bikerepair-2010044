<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Bike Repair |Profile</title>
		<meta charset="utf-8">

		<link rel="stylesheet" href="<?php echo base_url(); ?>css/reset.css" type="text/css" media="screen">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/style.css" type="text/css" media="screen">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/grid.css" type="text/css" media="screen">

		<link rel="stylesheet" href="<?php echo base_url(); ?>css/styleregistration.css" type="text/css" media="screen">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/demo.css" type="text/css" media="screen">

		<link rel="stylesheet" href="<?php echo base_url(); ?>css/reset.css" type="text/css" media="screen">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/style.css" type="text/css" media="screen">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/grid.css" type="text/css" media="screen">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/prettyPhoto.css" type="text/css" media="screen">
		<script src="<?php echo base_url(); ?>js/jquery-1.7.1.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/cufon-yui.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/cufon-replace.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/Vegur_500.font.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/FF-cash.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/jquery.prettyPhoto.js" type="text/javascript"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$(".lightbox").append("<span></span>");
				$("a[data-gal^='prettyVideo']").prettyPhoto({
					animation_speed : 'normal',
					theme : 'facebook',
					slideshow : false,
					autoplay_slideshow : false
				});
			});
		</script>
		<!--[if lt IE 9]>
		<script type="text/javascript" src="js/html5.js"></script>
		<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
		<![endif]-->
	</head>
	<body id="page3">
		<div class="main-bg">
			<div class="bg">
				<!--==============================header=================================-->
				<header>
					<div class="main">
						<div class="wrapper">
							<h1><a href="<?php echo base_url(); ?>index.php/site">Car Repair</a></h1>
							<div class="fright">
								<div class="indent">
									<span class="address">8901 Marmora Road, Glasgow, D04 89GR</span><span class="phone">Tel: +1 959 552 5963</span>
								</div>
							</div>
						</div>
						<nav>
							<ul class="menu">
									<li>
									<a class="active" href="<?php echo base_url(); ?>index.php/site">Home</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>index.php/questions">Questions</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>index.php/alltags/">Tags </a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>index.php/allusers/">Users</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>index.php/profile/">Profile</a>
								</li>


							</ul>
						</nav>
					</div>
				</header>
				<!--==============================content================================-->
				<div class="container">

					<div  class="form">

						<form id="profileform" name="profileform" action="/bikerepair/index.php/editprofile" >
							<p class="contact">
								<label for="name">Name</label>
							</p>
							<input id="name" name="name"  required="" tabindex="1" type="text" disabled="disabled">

							<p class="contact">
								<label for="email">Email</label>
							</p>
							<input id="email" name="email" required=""  type="email" disabled="disabled">

							<p class="contact">
								<label for="username">username</label>
							</p>
							<input id="username" name="username" placeholder="username"  required="" tabindex="2" type="text" disabled="disabled">

							<br>
							<br>

							<input class="buttom" name="editmyprofile" tabindex="5" value="Edit my profile" type="submit">
						</form>
					</div>

				</div><!-- container -->

				<!--==============================footer=================================-->
				<footer>
					<div class="main">
						<span>Copyright &copy; <a href="#">Domain Name</a> All Rights Reserved</span> Design by <a target="_blank" href="http://www.templatemonster.com/">TemplateMonster.com</a>
					</div>
				</footer>
			</div>
		</div>
		<script type="text/javascript">
			Cufon.now();

		</script>

		<script type="text/javascript">
			$(document).ready(function() {

//alert("script incoming");

//alert("getuserdetails incoming");

$.ajax({
type : 'GET',
async : false,
url : "<?php echo base_url(); ?>index.php/rest/resource/get_userdetails",
	dataType : "json",

	success : function(data) {
	console.log(data);

	//alert(data.name);

	$("#name").val(data.name);
	$("#username").val(data.username);
	$("#email").val(data.email);

	}
	});
	return false;

	// avoid to execute the actual submit of the form.
	//}

	});
		</script>

		<div align=center>
			This template  downloaded form <a href='http://all-free-download.com/free-website-templates/'>free website templates</a>
		</div>
	</body>
</html>