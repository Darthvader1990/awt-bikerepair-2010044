<!DOCTYPE html>
<html lang="en">
<head>
<title>Bike Repair |Ask Question</title>
<meta charset="utf-8">
<link rel="stylesheet" href="<?php echo base_url();?>css/reset.css" type="text/css" media="screen">
<link rel="stylesheet" href="<?php echo base_url();?>css/style.css" type="text/css" media="screen">
<link rel="stylesheet" href="<?php echo base_url();?>css/grid.css" type="text/css" media="screen">
<link rel="stylesheet" href="<?php echo base_url();?>css/jquery.tagsinput.css" type="text/css" media="screen">
<script src="<?php echo base_url();?>js/jquery-1.7.1.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/cufon-yui.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/cufon-replace.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/Vegur_500.font.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/FF-cash.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/tms-0.3.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/tms_presets.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/jquery.easing.1.3.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/jquery.equalheights.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/jquery.tagsinput.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/jquery.tagsinput.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#login-trigger').click(function(){
		$(this).next('#login-content').slideToggle();
		$(this).toggleClass('active');					
		
		if ($(this).hasClass('active')) $(this).find('span').html('&#x25B2;');
			else $(this).find('span').html('&#x25BC;');
		});
                
});		
			
</script>
<!--[if lt IE 9]>
<script type="text/javascript" src="js/html5.js"></script>
<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
<![endif]-->
</head>
<body id="page1">
<div class="main-bg">
  <div class="bg">
    <!--==============================header=================================-->
    <header>
      <div class="main">
        <div class="wrapper">
          <h1><a href="<?php echo base_url(); ?>index.php/site">Car Repair</a></h1>
          <div class="fright">
              <?php if($this->session->userdata('loggedIn') != 'True'){
                echo '<ul>
		<li id="login">
			<a id="login-trigger" href="#">
				Log in <span>▼</span>
			</a>
			<div id="login-content">
				<form id="loginForm" action="/bikerepair/index.php/site/login" method="post">
					<fieldset id="inputs">
                                            <select id="type" name="type">
                                                <option value="User">User</option>
                                                <option value="Staff">Staff</option>
                                            </select>
						<input id="username" type="text" name="username" placeholder="Username" required>   
						<input id="password" type="password" name="password" placeholder="Password" required>
					</fieldset>
					<fieldset id="actions">
						<input type="submit" id="submit" value="Log in">
					</fieldset>
				</form>
			</div>                     
		</li>
		<li class="signup">
			<a href="' . base_url() . 'index.php/auth/register">Sign up</a>
		</li>
	</ul> '; 
              }
              else{
               echo '<ul><li><b>  Welcome '.$this->session->userdata('name').'   </b></li>
                  <li class="signup">
			<a href="/bikerepair/index.php/auth/logout">Log Out</a>
		</li>
              </ul> '; 
              }
            
         ?>
              
          </div>
        </div>
        <nav>
          <ul class="menu">
            <li><a class="active" href="<?php echo base_url(); ?>index.php/site">Home</a></li>
            <li><a href="<?php echo base_url();?>index.php/site/questions">Questions</a></li>
            <li><a href="/bikerepair/index.php/maintenance/">Maintenance </a></li>
            <li><a href="repair.html">Repair</a></li>
            <li><a href="price.html">Price List</a></li>
            <li><a href="locations.html">Locations</a></li>
          </ul>
        </nav>
        
    </header>
    <!--==============================content================================-->
    <section id="content">
      <div class="main">
        <div class="container_12">
          <div class="wrapper p5">
            <h2><strong>Ask Question</strong></h2>
          </div>
          <div class="questionBox">
              <form action="<?php echo base_url();?>index.php/auth/postquestion" method="post" name="askQuestionForm">
                <table border="0" cellpadding="0">
  <tr>
    <td width="29">&nbsp;</td>
    <td width="161">&nbsp;</td>
    <td width="756">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="formalignment">Title</td>
    <td><input type="text" name="titleText" id="titleText"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="formalignment">Category</td>
    <td><select name="categorySet" id="categorySet">
   <?php 
   foreach ($cat->result() as $row){
    echo '<option value="'.$row->catid.'">'.$row->name.'</option>';   
   }
   
   ?>             
    </select></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="101">&nbsp;</td>
    <td class="formalignment">Question</td>
    <td><textarea name="questionBoxText" id="questionBoxText" cols="60" rows="10"></textarea></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="formalignment">Tags</td>
    <td><input type="text" name="tagsText" id="tagsText"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><input type="submit" name="button" id="button" value="Post"></td>
  </tr>
                </table>

                </form>
              
          </div>
        </div>
      </div>
    </section>
    <!--==============================footer=================================-->
    <footer>
      <div class="main"> <span>Copyright &copy; <a href="#">Domain Name</a> All Rights Reserved</span> Design by <a target="_blank" href="http://www.templatemonster.com/">TemplateMonster.com</a> </div>
    </footer>
  </div>
</div>
<script type="text/javascript">Cufon.now();</script>
<script type="text/javascript">
$(window).load(function () {
    
    $('#tagsText').tagsInput({width:'auto'});
});
</script>
<div align=center>This template  downloaded form <a href='http://all-free-download.com/free-website-templates/'>free website templates</a></div></body>
</html>