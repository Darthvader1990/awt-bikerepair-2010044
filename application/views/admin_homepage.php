<!DOCTYPE html>
<html lang="en">
	<head>

		<style>
			body {
				background: #555;
			}

			#search {

			}

			#search input[type="text"] {
				background: url(search-dark.png) no-repeat 10px 6px #444;
				border: 0 none;
				font: bold 12px Arial, Helvetica, Sans-serif;
				color: #777;
				width: 150px;
				padding: 6px 15px 6px 35px;
				-webkit-border-radius: 20px;
				-moz-border-radius: 20px;
				border-radius: 20px;
				text-shadow: 0 2px 2px rgba(0, 0, 0, 0.3);
				-webkit-box-shadow: 0 1px 0 rgba(255, 255, 255, 0.1), 0 1px 3px rgba(0, 0, 0, 0.2) inset;
				-moz-box-shadow: 0 1px 0 rgba(255, 255, 255, 0.1), 0 1px 3px rgba(0, 0, 0, 0.2) inset;
				box-shadow: 0 1px 0 rgba(255, 255, 255, 0.1), 0 1px 3px rgba(0, 0, 0, 0.2) inset;
				-webkit-transition: all 0.7s ease 0s;
				-moz-transition: all 0.7s ease 0s;
				-o-transition: all 0.7s ease 0s;
				transition: all 0.7s ease 0s;
			}

			#search input[type="text"]:focus {
				width: 200px;
			}

		</style>
		<title>Bike Repair</title>
		<meta charset="utf-8">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/reset.css" type="text/css" media="screen">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/style.css" type="text/css" media="screen">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/grid.css" type="text/css" media="screen">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/demo-style" type="text/css" media="screen">
		<script src="<?php echo base_url(); ?>js/jquery-1.7.1.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/cufon-yui.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/cufon-replace.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/Vegur_500.font.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/FF-cash.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/tms-0.3.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/tms_presets.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/jquery.easing.1.3.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/jquery.equalheights.js" type="text/javascript"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$('#login-trigger').click(function() {
					$(this).next('#login-content').slideToggle();
					$(this).toggleClass('active');

					if ($(this).hasClass('active'))
						$(this).find('span').html('&#x25B2;')
					else
						$(this).find('span').html('&#x25BC;')
				})
			});
		</script>

		<!--[if lt IE 9]>
		<script type="text/javascript" src="js/html5.js"></script>
		<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
		<![endif]-->
	</head>
	<body id="page1">
		<div class="main-bg">
			<div class="bg">
				<!--==============================header=================================-->
				<header>

					<div class="main">
						<div class="wrapper">
							<h1><a href="index.html">Car Repair</a></h1>

							<div class="fright">
								<?php
								if ($this -> session -> userdata('loggedIn') != 'True') {
									echo '<ul>

<li id="login">
<a id="login-trigger" href="#">
Log in <span>▼</span>
</a>
<div id="login-content">
<form id="loginForm" action="/bikerepair/index.php/auth/authenticate" method="post">
<fieldset id="inputs">
<input id="username" type="text" name="username" placeholder="Username" required>
<input id="password" type="password" name="password" placeholder="Password" required>
</fieldset>
<fieldset id="actions">
<input type="submit" id="submit" value="Log in">
</fieldset>
</form>
</div>
</li>
<li class="signup">
<a href="' . base_url() . 'index.php/auth/register">Sign up</a>
</li>
</ul> ';
								} else {
									echo '<ul><li><b>  Welcome ' . $this -> session -> userdata('name') . '   </b></li>
<li class="signup">
<a href="/bikerepair/index.php/auth/logout">Log Out</a>
</li>
</ul> ';
								}
								?>
							</div>
						</div>

						<form method="get" id="search">

							<input name="q" id= "search_text_id" type="text" size="40" placeholder="Search..." name="search" value="" id="some_name"/>

							<input type="submit" value="Submit">

						</form>

						</button>

						<nav>
							<ul class="menu">
								<li>
									<a class="active" href="<?php echo base_url(); ?>index.php">Home</a>
								</li>
								<li>
									<a href="/bikerepair/index.php/questions">Remove Q and A s</a>
								</li>
								
								<li>
									<a href="repair.html"> Remove Users</a>
								</li>
								<li>
									<a href="/bikerepair/index.php/profile/">Add Tags and categories</a>
								</li>

							</ul>
						</nav>
						<div class="slider-wrapper">
							<div class="slider">
								<ul class="items">
									<li>
										<img src="<?php echo base_url(); ?>images/slider-img1.jpg" alt="" />
									</li>
									<li>
										<img src="<?php echo base_url(); ?>images/slider-img2.jpg" alt="" />
									</li>
									<li>
										<img src="<?php echo base_url(); ?>images/slider-img3.jpg" alt="" />
									</li>
								</ul>
							</div>
							<a class="prev" href="#">prev</a><a class="next" href="#">next</a>
						</div>
					</div>
				</header>
				<!--==============================content================================-->
				<section id="content">
					<div class="main">
						<div class="container_12">
							<div class="wrapper p5">

							</div>
							<div class="container-bot">
								<div class="container-top">
									<div class="container">
										<div class="wrapper">
											<article class="grid_8">
												<div class="indent-left">

													<p class="prev-indent-bot">
														<strong>Car Repair</strong> is one of free website templates created by TemplateMonster.com team. This website template is optimized for 1280X1024 screen resolution. It is also XHTML &amp; CSS valid.
													</p>
													<p class="border-bot">
														This Car Repair Template goes with two packages – with PSD source files and without them. PSD source files are available for free for the registered members of TemplatesMonster.com. The basic package (without PSD source) is available for anyone without registration.
													</p>
												</div>
												<div class="wrapper">
													<div class="grid_4 alpha">
														<div class="indent-left">
															<div class="maxheight indent-bot">
																<h3>About Us</h3>
																<p class="prev-indent-bot">
																	<a class="link-1" href="#">Lorem ipsum dolor amet</a> conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.
																</p>
																<a class="link-1" href="#">Dolor amet conse ctetur</a> adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat ut enim ad.
															</div>
															<a class="button" href="#">Read More</a>
														</div>
													</div>
													<div class="grid_4 omega">
														<div class="indent-left2">
															<div class="maxheight indent-bot">
																<h3 class="p0">Our Services</h3>
																<ul class="list-1">
																	<li>
																		<a href="#">Complete Computer Diagnostics</a>
																	</li>
																	<li>
																		<a href="#">Complete Safety Analysis</a>
																	</li>
																	<li>
																		<a href="#">Drivability Problems</a>
																	</li>
																	<li>
																		<a href="#">Oil Changes</a>
																	</li>
																	<li>
																		<a href="#">Emission Repair Facility</a>
																	</li>
																	<li>
																		<a href="#">Air Conditioning Service</a>
																	</li>
																	<li>
																		<a href="#">Electrical Systems</a>
																	</li>
																	<li>
																		<a href="#">Fleet Maintenance</a>
																	</li>
																</ul>
															</div>
															<a class="button" href="#">Read More</a>
														</div>
													</div>
												</div>
											</article>
											<article class="grid_4">
												<div class="indent-left2 indent-top">
													<div class="box p4">
														<div class="padding">
															<div class="wrapper">
																<?php
																if ($this -> session -> userdata('loggedIn') === 'True' && $this -> session -> userdata('type') === 'student') {
																	echo '
<p class="p1">
<strong>Dashboard</strong>
</p>

<li>
<a href="' . base_url() . 'index.php/site/askquestion">Ask question</a>
</li>
<li>
<a href="#">My questions</a>
</li>';
																}
																?>
															</div>

														</div>
													</div>
													<figure class="indent-bot">
														<iframe width="260" height="202" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Brooklyn,+New+York,+NY,+United+States&amp;aq=0&amp;sll=37.0625,-95.677068&amp;sspn=61.282355,146.513672&amp;ie=UTF8&amp;hq=&amp;hnear=Brooklyn,+Kings,+New+York&amp;ll=40.649974,-73.950005&amp;spn=0.01628,0.025663&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe>
													</figure>
													<div class="indent-left">
														<dl class="main-address">
															<dt>
																Demolink.org 8901 Marmora Road,
																<br>
																Glasgow, D04 89GR.
															</dt>
															<dd>
																<span>Telephone:</span> +1 959 552 5963;
															</dd>
															<dd>
																<span>FAX:</span> +1 959 552 5963
															</dd>
															<dd>
																<span>E-mail:</span><a href="#">mail@demolink.org</a>
															</dd>
														</dl>
													</div>
												</div>
											</article>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!--==============================footer=================================-->
				<footer>
					<div class="main">
						<span>Copyright &copy; <a href="#">Domain Name</a> All Rights Reserved</span> Design by <a target="_blank" href="http://www.templatemonster.com/">TemplateMonster.com</a>
					</div>
				</footer>
			</div>
		</div>
		<script type="text/javascript">
			Cufon.now();
		</script>
		<script type="text/javascript">
			$(window).load(function() {
				$('.slider')._TMS({
					duration : 1000,
					easing : 'easeOutQuint',
					preset : 'simpleFade',
					slideshow : 7000,
					banners : false,
					pauseOnHover : true,
					pagination : false,
					pagNums : false,
					nextBu : '.next',
					prevBu : '.prev'
				});
			});
		</script>

		<script>
			$("#search").submit(function() {

				alert("button Incoming");
				
					var searchText = $('#search_text_id').val();

				$.ajax({
					type : 'GET',
					async : false,
					url : '<?php echo base_url(); ?>index.php/rest/resource/search_questions/title/' + searchText.trim(),
					dataType : "json",
					
					success : function(data) {
							console.log(data);
							
						$.each(data, function(i, item) {
						
							alert(item.title);

						});
					}
				});
				return false;

				// avoid to execute the actual submit of the form.
			});

			
		</script>
		
	

		<div align=center>
			This template  downloaded form <a href='http://all-free-download.com/free-website-templates/'>free website templates</a>
		</div>
	</body>
</html>