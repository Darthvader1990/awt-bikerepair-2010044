<!DOCTYPE html>
<html lang="en">
	<head>

		<style>
			body {
				background: #555;
			}

			#search {

			}

			#search input[type="text"] {
				background: url(search-dark.png) no-repeat 10px 6px #444;
				border: 0 none;
				font: bold 12px Arial, Helvetica, Sans-serif;
				color: #777;
				width: 150px;
				padding: 6px 15px 6px 35px;
				-webkit-border-radius: 20px;
				-moz-border-radius: 20px;
				border-radius: 20px;
				text-shadow: 0 2px 2px rgba(0, 0, 0, 0.3);
				-webkit-box-shadow: 0 1px 0 rgba(255, 255, 255, 0.1), 0 1px 3px rgba(0, 0, 0, 0.2) inset;
				-moz-box-shadow: 0 1px 0 rgba(255, 255, 255, 0.1), 0 1px 3px rgba(0, 0, 0, 0.2) inset;
				box-shadow: 0 1px 0 rgba(255, 255, 255, 0.1), 0 1px 3px rgba(0, 0, 0, 0.2) inset;
				-webkit-transition: all 0.7s ease 0s;
				-moz-transition: all 0.7s ease 0s;
				-o-transition: all 0.7s ease 0s;
				transition: all 0.7s ease 0s;
			}

			#search input[type="text"]:focus {
				width: 200px;
			}

		</style>
		<title>Bike Repair |Homepage</title>
		<meta charset="utf-8">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/reset.css" type="text/css" media="screen">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/style.css" type="text/css" media="screen">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/grid.css" type="text/css" media="screen">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/demo-style" type="text/css" media="screen">
		<script src="<?php echo base_url(); ?>js/jquery-1.7.1.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/cufon-yui.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/cufon-replace.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/Vegur_500.font.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/FF-cash.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/tms-0.3.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/tms_presets.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/jquery.easing.1.3.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/jquery.equalheights.js" type="text/javascript"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$('#login-trigger').click(function() {
					$(this).next('#login-content').slideToggle();
					$(this).toggleClass('active');

					if ($(this).hasClass('active'))
						$(this).find('span').html('&#x25B2;')
					else
						$(this).find('span').html('&#x25BC;')
				})
			});
		</script>

		<!--[if lt IE 9]>
		<script type="text/javascript" src="js/html5.js"></script>
		<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
		<![endif]-->
	</head>
	<body id="page1">
		<div class="main-bg">
			<div class="bg">
				<!--==============================header=================================-->
				<header>

					<div class="main">
						<div class="wrapper">
							<h1><a href="index.html">Bike Repair</a></h1>

							<div class="fright">
								<?php
								if ($this -> session -> userdata('loggedIn') != 'True') {
									echo '<ul>

<li id="login">
<a id="login-trigger" href="#">
Log in <span>▼</span>
</a>
<div id="login-content">
<form id="loginForm" action="/bikerepair/index.php/auth/authenticate" method="post">
<fieldset id="inputs">
<input id="username" type="text" name="username" placeholder="Username" required>
<input id="password" type="password" name="password" placeholder="Password" required>
</fieldset>
<fieldset id="actions">
<input type="submit" id="submit" value="Log in">
</fieldset>
</form>
</div>
</li>
<li class="signup">
<a href="' . base_url() . 'index.php/auth/register">Sign up</a>
</li>
</ul> ';
								} else {
									echo '<ul><li><b>  Welcome ' . $this -> session -> userdata('name') . '   </b></li>
<li class="signup">
<a href="/bikerepair/index.php/auth/logout">Log Out</a>
</li>
</ul> ';
								}
								?>
							</div>
						</div>

						<form method="get" id="search">

							<input name="q" id= "search_text_id" type="text" size="40" placeholder="Search..." name="search" value="" id="some_name"/>

							<input type="submit" value="Submit">

						</form>

						</button>

						<nav>
							<ul class="menu">
									<li>
									<a class="active" href="<?php echo base_url(); ?>index.php/site">Home</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>index.php/questions">Questions</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>index.php/alltags/">Tags </a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>index.php/allusers/">Users</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>index.php/profile/">Profile</a>
								</li>


							</ul>
						</nav>
						<div class="slider-wrapper">
							<div class="slider">
								<ul class="items">
									<li>
										<img src="<?php echo base_url(); ?>images/slider-img1.jpg" alt="" />
									</li>
									<li>
										<img src="<?php echo base_url(); ?>images/slider-img2.jpg" alt="" />
									</li>
									<li>
										<img src="<?php echo base_url(); ?>images/slider-img3.jpg" alt="" />
									</li>
								</ul>
							</div>
							<a class="prev" href="#">prev</a><a class="next" href="#">next</a>
						</div>
					</div>
				</header>
				<!--==============================content================================-->
				<section id="content">
					<div class="main">
						<div class="container_12">
							<div class="wrapper p5">

							</div>
							<div class="container-bot">
								<div class="container-top">
									<div class="container">
										<div class="wrapper">

											<article class="grid_4">
												<div class="indent-left2 indent-top">
													<div class="box p4">
														<div class="padding">

															<table id="search_question_table" width="100%" >

																<tbody>
																	<tr>

																	</tr>
																</tbody>
															</table>
															<div class="wrapper">
																<?php
																if ($this -> session -> userdata('loggedIn') === 'True' && $this -> session -> userdata('type') === 'student') {
																	echo '
<p class="p1">
<strong>Dashboard</strong>
</p>

<li>
<a href="' . base_url() . 'index.php/site/askquestion">Ask question</a>
</li>
<li>
<a href="#">My questions</a>
</li>';
																}
																?>
															</div>

														</div>
													</div>
													
													<div class="indent-left">

													</div>
												</div>
											</article>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!--==============================footer=================================-->
				<footer>
					<div class="main">
						<span>Copyright &copy; <a href="#">Domain Name</a> All Rights Reserved</span> Design by <a target="_blank" href="http://www.templatemonster.com/">TemplateMonster.com</a>
					</div>
				</footer>
			</div>
		</div>
		<script type="text/javascript">
			Cufon.now();
		</script>
		<script type="text/javascript">
			$(window).load(function() {
				$('.slider')._TMS({
					duration : 1000,
					easing : 'easeOutQuint',
					preset : 'simpleFade',
					slideshow : 7000,
					banners : false,
					pauseOnHover : true,
					pagination : false,
					pagNums : false,
					nextBu : '.next',
					prevBu : '.prev'
				});
			});
		</script>

		<script>
			$("#search").submit(function() {

alert("button Incoming");

var searchText = $('#search_text_id').val();

document.getElementById("search_question_table").innerHTML = "";
var HTML='';
$.ajax({
type : 'GET',
async : false,
url : '<?php echo base_url(); ?>index.php/rest/resource/search_questions/title/' + searchText.trim(),
dataType : "json",

success : function(data) {
console.log(data);

$.each(data, function(i, item) {

alert(item.title);
HTML += '<tr><td>' + '<a href="/Codeigniter_first/index.php/question/questionView?id=' + item.questionid + '">' + item.title + '</td></tr>';

});
$('#search_question_table').append(HTML);
}
});

return false;

// avoid to execute the actual submit of the form.
});

		</script>

		<div align=center>
			This template  downloaded form <a href='http://all-free-download.com/free-website-templates/'>free website templates</a>
		</div>
	</body>
</html>