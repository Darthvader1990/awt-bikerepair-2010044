<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Bike Repair |Questions</title>
		<meta charset="utf-8">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/reset.css" type="text/css" media="screen">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/style.css" type="text/css" media="screen">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/grid.css" type="text/css" media="screen">
		<script src="<?php echo base_url(); ?>js/jquery-1.7.1.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/cufon-yui.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/cufon-replace.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/Vegur_500.font.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/FF-cash.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/tms-0.3.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/tms_presets.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/jquery.easing.1.3.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/jquery.equalheights.js" type="text/javascript"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$('#login-trigger').click(function() {
					$(this).next('#login-content').slideToggle();
					$(this).toggleClass('active');

					if ($(this).hasClass('active'))
						$(this).find('span').html('&#x25B2;');
					else
						$(this).find('span').html('&#x25BC;');
				});
			});
                        
                        
		</script>
		<!--[if lt IE 9]>
		<script type="text/javascript" src="js/html5.js"></script>
		<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
		<![endif]-->
	</head>
	<body id="page1">
		<div class="main-bg">
			<div class="bg">
				<!--==============================header=================================-->
				<header>
					<div class="main">
						<div class="wrapper">
							<h1><a href="<?php echo base_url(); ?>index.php/site">Car Repair</a></h1>
							<div class="fright">
								<?php
								if ($this -> session -> userdata('loggedIn') != 'True') {
									echo '<ul>
<li id="login">
<a id="login-trigger" href="#">
Log in <span>▼</span>
</a>
<div id="login-content">
<form id="loginForm" action="/bikerepair/index.php/site/login" method="post">
<fieldset id="inputs">

<input id="username" type="text" name="username" placeholder="Username" required>
<input id="password" type="password" name="password" placeholder="Password" required>
</fieldset>
<fieldset id="actions">
<input type="submit" id="submit" value="Log in">
</fieldset>
</form>
</div>
</li>
<li class="signup">
<a href="' . base_url() . 'index.php/auth/register">Sign up</a>
</li>
</ul> ';
								} else {
									echo '<ul><li><b>  Welcome ' . $this -> session -> userdata('name') . '   </b></li>
<li class="signup">
<a href="/bikerepair/index.php/auth/logout">Log Out</a>
</li>
</ul> ';
								}
								?>
							</div>
						</div>
						<nav>
							<ul class="menu">
									<li>
									<a class="active" href="<?php echo base_url(); ?>index.php/site">Home</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>index.php/questions">Questions</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>index.php/alltags/">Tags </a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>index.php/allusers/">Users</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>index.php/profile/">Profile</a>
								</li>


							</ul>
						</nav>

				</header>
				<!--==============================content================================-->
				<section id="content">
					<div class="main">
						<div class="container_12">
                                                    <div class="wrapper p5">
                                                        <h2><strong><?php echo $ques['question']->row()->title;?></strong></h2>
                                                        <div id="questionBody">
                                                            <div id="question"><?php echo $ques['question']->row()->question;?></div>
                                                            <div id="userinfo">
                                                                <div id="date"><?php echo $ques['question']->row()->date;?></div>
                                                                <div id="user"><?php echo $ques['question']->row()->userid;?></div>
                                                            </div>
                                                        </div>  
                                                        
                                                        <div id="answerSection">
                                                            <div id="answerscount"><?php echo $ques['answers']->num_rows(); if($ques['answers']->num_rows() == '1')echo ' Answer'; else echo ' Answers'; ?></div>
                                                            <?php foreach($ques['answers']->result() as $row){
                                                                echo '<div id="answersBody">
                                                            <div class="box1">
                                                            <div class="up"><a href="" class="vote" name ="up" id="'.$row->answerid.'"><img src="'.  base_url().'images/thumbsUp.jpg" width="40" height="40"></a></div>
                                                                <div class="upScore" id="upScore'.$row->answerid.'">+'.$row->ratingup.'</div>
                                                            <div class="down"><a href="" class="vote" name ="down" id="'.$row->answerid.'"><img src="'.  base_url().'images/thumbsDown.jpg" width="40" height="40"></a></div>
                                                                <div class="downScore" id="downScore">-'.$row->ratingdown.'</div>
                                                            </div>
                                                            
                                                            <div id="answer">'.$row->answer.'</div>
                                                            <div id="Auserinfo">
                                                                <div id="Adate">answered '.$row->date.'</div>
                                                                <div id="Auser">'.$row->username.'</div>
                                                            </div>
                                                            <div class="line"></div>
                                                        </div>';
                                                            }
                                                            ?>
                                                        
                                                        </div>
                                                        
                                                    </div>

							
						</div>
                                            <?php if($this->session->userdata('loggedIn') == 'True' && $this->session->userdata('type') == 'mechanic'){
                                              echo '<div id="answerArea">
                                                <div id="Ans">Answer</div>
                                                <form action="'.  base_url().'index.php/question/postanswer/'.$ques['question']->row()->questionid.'" method="post">
                                                    <textarea id="writeAnswer" name="writeAnswer" cols="80" rows="10"></textarea>
                                                           
                                                    <input id="PostAnswer" name="PostAnswer" type="submit" value="Post Your Answer">
                                                </form>
                                            </div>'  ;
                                            }
                                             ?>
                                            
                                            
					</div>
				</section>
				<!--==============================footer=================================-->
				<footer>
					<div class="main">
						<span>Copyright &copy; <a href="#">Domain Name</a> All Rights Reserved</span> Design by <a target="_blank" href="http://www.templatemonster.com/">TemplateMonster.com</a>
					</div>
				</footer>
			</div>
		</div>
		<script type="text/javascript">
			Cufon.now();
		</script>
                <script type="text/javascript">
                        $(function() {
$(".vote").click(function() 
{   

var id = $(this).attr("id");
var name = $(this).attr("name");


$.ajax({
type: "POST",
url: "<?php echo base_url();?>index.php/question/vote",
data: {id:id,voteType:name},
cache: false,

success: function(msg)
{
      if(name ==='up'){
     $("#upScore"+id).fadeOut("fast"); 
     $("#upScore"+id).html(msg);
     $("#upScore"+id).fadeIn();
    }
    else{
        $("#downScore").fadeOut("fast"); 
        $("#downScore").html(msg);
        $("#downScore").fadeIn();
    }
     
    

} 
});


return false;
});
});
                </script>
		<div align=center>
			This template  downloaded form <a href='http://all-free-download.com/free-website-templates/'>free website templates</a>
		</div>
	</body>
</html>