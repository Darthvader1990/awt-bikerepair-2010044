<!DOCTYPE html>
<html lang="en">
	<head>
		
		<style>
			body {
				background: #555;
			}

			#search {

			}

			#search input[type="text"] {
				background: url(search-dark.png) no-repeat 10px 6px #444;
				border: 0 none;
				font: bold 12px Arial, Helvetica, Sans-serif;
				color: #777;
				width: 150px;
				padding: 6px 15px 6px 35px;
				-webkit-border-radius: 20px;
				-moz-border-radius: 20px;
				border-radius: 20px;
				text-shadow: 0 2px 2px rgba(0, 0, 0, 0.3);
				-webkit-box-shadow: 0 1px 0 rgba(255, 255, 255, 0.1), 0 1px 3px rgba(0, 0, 0, 0.2) inset;
				-moz-box-shadow: 0 1px 0 rgba(255, 255, 255, 0.1), 0 1px 3px rgba(0, 0, 0, 0.2) inset;
				box-shadow: 0 1px 0 rgba(255, 255, 255, 0.1), 0 1px 3px rgba(0, 0, 0, 0.2) inset;
				-webkit-transition: all 0.7s ease 0s;
				-moz-transition: all 0.7s ease 0s;
				-o-transition: all 0.7s ease 0s;
				transition: all 0.7s ease 0s;
			}

			#search input[type="text"]:focus {
				width: 200px;
			}

		</style>
		<title>Bike Repair</title>
		<meta charset="utf-8">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/reset.css" type="text/css" media="screen">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/style.css" type="text/css" media="screen">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/grid.css" type="text/css" media="screen">

		<link rel="stylesheet" href="<?php echo base_url(); ?>css/styleregistration.css" type="text/css" media="screen">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/demo.css" type="text/css" media="screen">

		<script src="<?php echo base_url(); ?>js/jquery-1.7.1.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/cufon-yui.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/cufon-replace.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/Vegur_500.font.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/FF-cash.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/tms-0.3.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/tms_presets.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/jquery.easing.1.3.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/jquery.equalheights.js" type="text/javascript"></script>

		<script type="text/javascript">
			$(document).ready(function() {
				$('#login-trigger').click(function() {
					$(this).next('#login-content').slideToggle();
					$(this).toggleClass('active');

					if ($(this).hasClass('active'))
						$(this).find('span').html('&#x25B2;');
					else
						$(this).find('span').html('&#x25BC;');
				});
			});

		</script>
		<!--[if lt IE 9]>
		<script type="text/javascript" src="js/html5.js"></script>
		<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
		<![endif]-->

		<!-- General Metas -->
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<!-- Force Latest IE rendering engine -->
		<title>Login Form</title>
		<meta name="description" content="">
		<meta name="author" content="">
		<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<!-- Mobile Specific Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

		<!-- Stylesheets -->
		<link rel="stylesheet" href="css/base.css">
		<link rel="stylesheet" href="css/skeleton.css">
		<link rel="stylesheet" href="css/layout.css">
	</head>
	<body id="page1">
		<div class="main-bg">
			<div class="bg">
				<!--==============================header=================================-->
				<header>
					<div class="main">
						<div class="wrapper">
							<h1><a href="index.html">Car Repair</a></h1>
							<div class="fright">
								<?php
								if ($this -> session -> userdata('loggedIn') != 'True') {
									echo '<ul>
<li id="login">
<a id="login-trigger" href="#">
Log in <span>▼</span>
</a>
<div id="login-content">
<form id="loginForm" action="' . base_url() . 'index.php/site/login" method="post">
<fieldset id="inputs">
<select id="type" name="type">
<option value="User">User</option>
<option value="Staff">Staff</option>
</select>
<input id="username" type="text" name="username" placeholder="Username" required>
<input id="password" type="password" name="password" placeholder="Password" required>
</fieldset>
<fieldset id="actions">
<input type="submit" id="submit" value="Log in">
</fieldset>
</form>
</div>
</li>
<li class="signup">
<a href="#">Sign up</a>
</li>
</ul> ';
								} else {
									echo '<ul><li><b>  Welcome ' . $this -> session -> userdata('name') . '   </b></li>
<li class="signup">
<a href="/bikerepair/index.php/site/logout">Log Out</a>
</li>
</ul> ';
								}
								?>
							</div>
						</div>
						<nav>
							<ul class="menu">
								<li>
									<a class="active" href="<?php echo base_url(); ?>index.php">Home</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>index.php/questions">Remove Q and A s</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>index.php/maintenance/">Remove Users</a>
								</li>
								<li>
									<a href="repair.html">Add Tags and categories</a>
								</li>
								
								
							</ul>
						</nav>

				</header>
				<!--==============================content================================-->
				<section id="content">
					<!--<div class="main">
					<div class="container_12">

					<div class="notice">
					<a href="" class="close">close</a>
					<p class="warn">Whoops! We didn't recognise your username or password. Please try again.</p>
					</div>-->

					<!-- Primary Page Layout -->

					<div class="container">

						<div  class="form">
                                                  <form id="profileform" name="profileform" action="/bikerepair/index.php/editprofile" >
							
							
							 <div id="questionBody">
                                                            <div id="question"><?php echo $ques -> question; ?></div>
                                                            <div id="userinfo">
                                                                <div id="date"><?php echo $ques -> date; ?></div>
                                                                <div id="user"><?php echo $ques -> userid; ?></div>
                                                            </div>
                                                        </div>  

							<input class="buttom" name="removeprofile" tabindex="5" value="Remove user" type="submit">
						</form>
								
							</form>
                                                    
						</div>

					</div><!-- container -->
				</section>
				<!--==============================footer=================================-->
				<footer>
					<div class="main">
						<span>Copyright &copy; <a href="#">Domain Name</a> All Rights Reserved</span> Design by <a target="_blank" href="http://www.templatemonster.com/">TemplateMonster.com</a>
					</div>
				</footer>
			</div>
		</div>
		<script type="text/javascript">
			Cufon.now();
		</script>
		<script type="text/javascript">
			$(window).load(function() {
				$('.slider')._TMS({
					duration : 1000,
					easing : 'easeOutQuint',
					preset : 'simpleFade',
					slideshow : 7000,
					banners : false,
					pauseOnHover : true,
					pagination : false,
					pagNums : false,
					nextBu : '.next',
					prevBu : '.prev'
				});
			});
		</script>
		<div align=center>
			This template  downloaded form <a href='http://all-free-download.com/free-website-templates/'>free website templates</a>
		</div>
	</body>
</html>