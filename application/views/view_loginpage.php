<!DOCTYPE html>
<html lang="en">
<head>
<title>Bike Repair |Login</title>
<meta charset="utf-8">
<link rel="stylesheet" href="<?php echo base_url();?>css/reset.css" type="text/css" media="screen">
<link rel="stylesheet" href="<?php echo base_url();?>css/style.css" type="text/css" media="screen">
<link rel="stylesheet" href="<?php echo base_url();?>css/grid.css" type="text/css" media="screen">

<link rel="stylesheet" href="<?php echo base_url();?>css/base.css" type="text/css" media="screen">
<link rel="stylesheet" href="<?php echo base_url();?>css/skeleton.css" type="text/css" media="screen">
<link rel="stylesheet" href="<?php echo base_url();?>css/layout.css" type="text/css" media="screen">


		

<script src="<?php echo base_url();?>js/jquery-1.7.1.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/cufon-yui.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/cufon-replace.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/Vegur_500.font.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/FF-cash.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/tms-0.3.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/tms_presets.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/jquery.easing.1.3.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/jquery.equalheights.js" type="text/javascript"></script>

<script src="<?php echo base_url();?>js/jquery.jquery-1.5.1.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/jquery.app.js" type="text/javascript"></script>




<script type="text/javascript">
$(document).ready(function(){
	$('#login-trigger').click(function(){
		$(this).next('#login-content').slideToggle();
		$(this).toggleClass('active');					
		
		if ($(this).hasClass('active')) $(this).find('span').html('&#x25B2;')
			else $(this).find('span').html('&#x25BC;')
		})
});
</script>
<!--[if lt IE 9]>
<script type="text/javascript" src="js/html5.js"></script>
<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
<![endif]-->

	<!-- General Metas -->
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">	<!-- Force Latest IE rendering engine -->
	<title>Login Form</title>
	<meta name="description" content="">
	<meta name="author" content="">
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" /> 
	
	<!-- Stylesheets -->
	<link rel="stylesheet" href="css/base.css">
	<link rel="stylesheet" href="css/skeleton.css">
	<link rel="stylesheet" href="css/layout.css">
</head>
<body id="page1">
<div class="main-bg">
  <div class="bg">
    <!--==============================header=================================-->
    <header>
      <div class="main">
        <div class="wrapper">
          <h1><a href="index.html">Car Repair</a></h1>
          
        </div>
        <nav>
          <ul class="menu">
           <li>
									<a class="active" href="<?php echo base_url(); ?>index.php/site">Home</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>index.php/questions">Questions</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>index.php/alltags/">Tags </a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>index.php/allusers/">Users</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>index.php/profile/">Profile</a>
								</li>
            
            
          </ul>
        </nav>
        
    </header>
    <!--==============================content================================-->
    <section id="content">
      <!--<div class="main">
        <div class="container_12">
         
            
      <div class="notice">
		<a href="" class="close">close</a>
		<p class="warn">Whoops! We didn't recognise your username or password. Please try again.</p>
	</div>-->



	<!-- Primary Page Layout -->

	<div class="container">
		
		<div class="form-bg">
			<form action="/bikerepair/index.php/auth/authenticate" method="post">
				<h2>Login</h2>
                                <div id="logingError">&nbsp;&nbsp;<?php echo $errmsg; ?></div>
				<p><input id="username" type="text" name="username" placeholder="Username" required></p>
				<p><input id="password" type="password" name="password" placeholder="Password" required></p>
				
				<button type="submit"></button>
			</form>
                    
		</div>

                
		<p class="forgot">Forgot your password? <a href="">Click here to reset it.</a></p>


	</div><!-- container -->
    </section>
    <!--==============================footer=================================-->
    <footer>
      <div class="main"> <span>Copyright &copy; <a href="#">Domain Name</a> All Rights Reserved</span> Design by <a target="_blank" href="http://www.templatemonster.com/">TemplateMonster.com</a> </div>
    </footer>
  </div>
</div>
<script type="text/javascript">Cufon.now();</script>
<script type="text/javascript">
$(window).load(function () {
    $('.slider')._TMS({
        duration: 1000,
        easing: 'easeOutQuint',
        preset: 'simpleFade',
        slideshow: 7000,
        banners: false,
        pauseOnHover: true,
        pagination: false,
        pagNums: false,
        nextBu: '.next',
        prevBu: '.prev'
    });
});
</script>
<div align=center>This template  downloaded form <a href='http://all-free-download.com/free-website-templates/'>free website templates</a></div></body>
</html>