<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Bike Repair |Questions</title>
		<meta charset="utf-8">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/reset.css" type="text/css" media="screen">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/style.css" type="text/css" media="screen">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/grid.css" type="text/css" media="screen">
		<script src="<?php echo base_url(); ?>js/jquery-1.7.1.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/cufon-yui.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/cufon-replace.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/Vegur_500.font.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/FF-cash.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/tms-0.3.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/tms_presets.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/jquery.easing.1.3.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/jquery.equalheights.js" type="text/javascript"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$('#login-trigger').click(function() {
					$(this).next('#login-content').slideToggle();
					$(this).toggleClass('active');

					if ($(this).hasClass('active'))
						$(this).find('span').html('&#x25B2;')
					else
						$(this).find('span').html('&#x25BC;')
				})
			});
		</script>
		<!--[if lt IE 9]>
		<script type="text/javascript" src="js/html5.js"></script>
		<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
		<![endif]-->
	</head>
	<body id="page1">
		<div class="main-bg">
			<div class="bg">
				<!--==============================header=================================-->
				<header>
					<div class="main">
						<div class="wrapper">
							<h1><a href="<?php echo base_url(); ?>index.php/site">Car Repair</a></h1>
							<div class="fright">
								<?php
								if ($this -> session -> userdata('loggedIn') != 'True') {
									echo '<ul>
<li id="login">
<a id="login-trigger" href="#">
Log in <span>▼</span>
</a>
<div id="login-content">
<form id="loginForm" action="/bikerepair/index.php/site/login" method="post">
<fieldset id="inputs">

<input id="username" type="text" name="username" placeholder="Username" required>
<input id="password" type="password" name="password" placeholder="Password" required>
</fieldset>
<fieldset id="actions">
<input type="submit" id="submit" value="Log in">
</fieldset>
</form>
</div>
</li>
<li class="signup">
<a href="' . base_url() . 'index.php/auth/register">Sign up</a>
</li>
</ul> ';
								} else {
									echo '<ul><li><b>  Welcome ' . $this -> session -> userdata('name') . '   </b></li>
<li class="signup">
<a href="/bikerepair/index.php/auth/logout">Log Out</a>
</li>
</ul> ';
								}
								?>
							</div>
						</div>
						<nav>
							<ul class="menu">
									<li>
									<a class="active" href="<?php echo base_url(); ?>index.php/site">Home</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>index.php/questions">Questions</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>index.php/alltags/">Tags </a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>index.php/allusers/">Users</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>index.php/profile/">Profile</a>
								</li>


							</ul>
						</nav>

				</header>
				<!--==============================content================================-->
				<section id="content">
					<div class="main">
						<div class="container_12">
							<div class="wrapper p5">
								<h2><strong>Questions</strong></h2>
							</div>

							<?php
	foreach ($ques->result() as $row) {
		echo '<div class="searchResult">
<div class="date">posted ' . $row -> date . ' ago</div>
<div class="searchTitle">
<a href="'.  base_url().'index.php/question/getquestion/'.$row->questionid.'">' . $row -> title . '</a>
</div>
<div class="answers">' . $row -> answers . ' Answer(s)</div>

<div class="username">asked by ' . $row -> username . '</div>
<div class="line"></div>
</div>';
	}
							?>
						</div>
					</div>
				</section>
				<!--==============================footer=================================-->
				<footer>
					<div class="main">
						<span>Copyright &copy; <a href="#">Domain Name</a> All Rights Reserved</span> Design by <a target="_blank" href="http://www.templatemonster.com/">TemplateMonster.com</a>
					</div>
				</footer>
			</div>
		</div>
		<script type="text/javascript">
			Cufon.now();
		</script>
		<script type="text/javascript">
			$(window).load(function() {
				$('.slider')._TMS({
					duration : 1000,
					easing : 'easeOutQuint',
					preset : 'simpleFade',
					slideshow : 7000,
					banners : false,
					pauseOnHover : true,
					pagination : false,
					pagNums : false,
					nextBu : '.next',
					prevBu : '.prev'
				});
			});
		</script>
		<div align=center>
			This template  downloaded form <a href='http://all-free-download.com/free-website-templates/'>free website templates</a>
		</div>
	</body>
</html>