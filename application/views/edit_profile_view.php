<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Bike Repair |Edit Profile</title>
		<meta charset="utf-8">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/reset.css" type="text/css" media="screen">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/style.css" type="text/css" media="screen">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/grid.css" type="text/css" media="screen">

		<link rel="stylesheet" href="<?php echo base_url(); ?>css/styleregistration.css" type="text/css" media="screen">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/demo.css" type="text/css" media="screen">

		<script src="<?php echo base_url(); ?>js/jquery-1.7.1.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/cufon-yui.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/cufon-replace.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/Vegur_500.font.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/FF-cash.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/tms-0.3.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/tms_presets.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/jquery.easing.1.3.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/jquery.equalheights.js" type="text/javascript"></script>

		<script>
			function validateForm() {

				var currnt_pswd = document.forms["editprofileform"]["current_pwd"].value;
				var new_pswd = document.forms["editprofileform"]["new_pwd"].value;
				var cnfrm_new_pswd = document.forms["editprofileform"]["cnfm_new_pwd"].value;

				if (currnt_pswd == null || currnt_pswd == "") {

					alert("Please enter your current passwrd");
					return false;
				}
				if (new_pswd != cnfrm_new_pswd) {

					alert("Please confrim your new password");
					return false;
				}

			}

		</script>

		- See more at: http://www.w3resource.com/javascript/form/javascript-sample-registration-form-validation.php#sthash.ZOjHtWcq.dpuf
		<script type="text/javascript">
			$(document).ready(function() {
				$('#login-trigger').click(function() {
					$(this).next('#login-content').slideToggle();
					$(this).toggleClass('active');

					if ($(this).hasClass('active'))
						$(this).find('span').html('&#x25B2;');
					else
						$(this).find('span').html('&#x25BC;');
				});
			});

		</script>

		<!--[if lt IE 9]>
		<script type="text/javascript" src="js/html5.js"></script>
		<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
		<![endif]-->

		<!-- General Metas -->
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<!-- Force Latest IE rendering engine -->
		<title>Login Form</title>
		<meta name="description" content="">
		<meta name="author" content="">
		<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<!-- Mobile Specific Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

		<!-- Stylesheets -->
		<link rel="stylesheet" href="css/base.css">
		<link rel="stylesheet" href="css/skeleton.css">
		<link rel="stylesheet" href="css/layout.css">
	</head>
	<body id="page1">

		<div class="main-bg">
			<div class="bg">
				<!--==============================header=================================-->
				<header>
					<div class="main">
						<div class="wrapper">
							<h1><a href="index.html">Car Repair</a></h1>
							<div class="fright">
								<?php
								if ($this -> session -> userdata('loggedIn') != 'True') {
									echo '<ul>
<li id="login">
<a id="login-trigger" href="#">
Log in <span>▼</span>
</a>
<div id="login-content">
<form id="loginForm" action="' . base_url() . 'index.php/site/login" method="post">
<fieldset id="inputs">
<select id="type" name="type">
<option value="User">User</option>
<option value="Staff">Staff</option>
</select>
<input id="username" type="text" name="username" placeholder="Username" required>
<input id="password" type="password" name="password" placeholder="Password" required>
</fieldset>
<fieldset id="actions">
<input type="submit" id="submit" value="Log in">
</fieldset>
</form>
</div>
</li>
<li class="signup">
<a href="#">Sign up</a>
</li>
</ul> ';
								} else {
									echo '<ul><li><b>  Welcome ' . $this -> session -> userdata('name') . '   </b></li>
<li class="signup">
<a href="/bikerepair/index.php/site/logout">Log Out</a>
</li>
</ul> ';
								}
								?>
							</div>
						</div>
						<nav>
							<ul class="menu">
								<li>
									<a class="active" href="<?php echo base_url(); ?>index.php">Home</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>index.php/questions">Questions</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>index.php/maintenance/">Tags </a>
								</li>
								<li>
									<a href="repair.html">Users</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>index.php/profile/">Profile</a>
								</li>
								<li>

								</li>
							</ul>
						</nav>

				</header>
				<!--==============================content================================-->
				<section id="content">
					<!--<div class="main">
					<div class="container_12">

					<div class="notice">
					<a href="" class="close">close</a>
					<p class="warn">Whoops! We didn't recognise your username or password. Please try again.</p>
					</div>-->

					<!-- Primary Page Layout -->

					<div class="container">

						<div  class="form">
							<span style="color: red; font-weight: bolder"> </span>
							<br>
							<form id="editprofileform" name="edit_profile_form" action="" method="post">

								<p class="contact">
									<label for="current_password">Current Password </label><span id="errorMes"></span>
								</p>
								<input id="current_pwd" name="current_pwd"  tabindex="2" type="password">

								<p class="contact">
									<label for="password">New password</label>
								</p>
								<input type="password" id="new_pwd" name="password" required="">
								<p class="contact">
									<label for="new_cnfm_psd">Confirm your password </label>
								</p>

								<input type="password" id="cnfm_new_pwd" name="cnfm_new_pwd" required="">

								<br>
								<br>
								<input class="buttom" name="edit_prof" tabindex="5" value="Save" type="submit" onclick="validateForm()">
							</form>

						</div>

					</div><!-- container -->
				</section>
				<!--==============================footer=================================-->
				<footer>
					<div class="main">
						<span>Copyright &copy; <a href="#">Domain Name</a> All Rights Reserved</span> Design by <a target="_blank" href="http://www.templatemonster.com/">TemplateMonster.com</a>
					</div>
				</footer>
			</div>
		</div>
		<script type="text/javascript">
			Cufon.now();
		</script>
		<script type="text/javascript">
			$(window).load(function() {
				$('.slider')._TMS({
					duration : 1000,
					easing : 'easeOutQuint',
					preset : 'simpleFade',
					slideshow : 7000,
					banners : false,
					pauseOnHover : true,
					pagination : false,
					pagNums : false,
					nextBu : '.next',
					prevBu : '.prev'
				});
			});
		</script>

		<script>
			$("#edit_prof").submit(function() {

alert("button Incoming");

//var newpassword = $('#new_pwd').val();


$.ajax({
type : 'POST',
async : false,
url : "<?php echo base_url(); ?>index.php/rest/resource/edit_userdetails",
	data:jsonData,
	Data = {password: password}
	dataType : "json",

	success : function(data) {
	console.log(data);

	}
	});
	return false;

	// avoid to execute the actual submit of the form.
	});

		</script>
		<div align=center>
			This template  downloaded form <a href='http://all-free-download.com/free-website-templates/'>free website templates</a>
		</div>
	</body>
</html>