<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class search_model extends CI_Model {

	function __construct() {

		parent::__construct();
		$this -> load -> database();

	}

	public function getQuestions($args) {
		/*function getTitle($search)
		 {
		 $this->db->where('title', $search);
		 $query = $this->db->get('question');
		 return $query->result();
		 }*/

		//$this -> db -> select('questions');
		$this -> db -> from('questions');
	//	$this -> db -> join('tags', 'questions.questionid = tag.questionid', 'INNER');

		//$this -> db -> where_in('tags.name', $array);
		$this -> db -> like('title', $args['title']);
		$query = $this -> db -> get();
		$rows_array = $query -> result_array();
		return $rows_array;

		/*$result = array();
		 $this -> db -> select('*');
		 $this -> db -> where('questions', $userInput);
		 $dh = $this -> db -> get('question');

		 foreach ($dh->result() as $value) {
		 $result[] = $value;
		 }

		 return $result;
		 * */
	}

	/*public function getAllQues() {
	 $query = $this -> db -> query("SELECT qa.questionid, qa.title,u.username, DATE_FORMAT(qa.questiondate,'%b %d %Y %h:%i %p') as 'date',count(qa.answerid) as 'answers',u.loyalty FROM questionsanswers qa, user u where qa.userid=u.userid group by qa.questionid");

	 return $query;
	 }

	 public function get($id) {
	 $query = $this -> db -> query('select * from questions where questionid=' . $id . '');

	 return $query -> result();
	 }

	 public function getCat() {

	 return $this -> db -> query('select * from category');
	 }
	 */

}
?>
