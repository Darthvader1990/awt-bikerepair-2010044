<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Questions_model extends CI_Model {

    function __construct() {
        
    }

    public function getAllQues() {
        //$query = $this->db->query("SELECT qa.questionid, qa.title,u.username, DATE_FORMAT(qa.questiondate,'%b %d %Y %h:%i %p') as 'date',count(qa.answerid) as 'answers',u.loyalty FROM questionsanswers qa, user u where qa.userid=u.userid group by qa.questionid");
        $query = $this->db->query("SELECT qa.questionid, qa.title,u.username, customdatediff(qa.questiondate) as 'date',count(qa.answerid) as 'answers',u.loyalty FROM questionsanswers qa, user u where qa.userid=u.userid group by qa.questionid");

        return $query;
    }

    public function get($id) {
        $query = $this->db->query('select * from questions where questionid=' . $id . '');


        return $query->result();
    }

    public function getCat() {

        return $this->db->query('select * from category');
    }

    public function addVote($answerid,$type,$userid){
        
        $check = $this->db->query('select * from voting where userid = '.$userid.' and answerid='.$answerid.'');
        if($check->num_rows() != 1){
        $values = array('answerid'=>$answerid,'userid'=>$userid,'voting'=>$type);
        $this->db->insert('voting',$values);
        
        if($type == 'up'){
            $this->db->where('answerid',$answerid);
            $this->db->set('ratingup', 'ratingup+1', FALSE);
            $this->db->update('answers');
        }
        else{
            $this->db->where('answerid',$answerid);
            $this->db->set('ratingdown', 'ratingdown+1', FALSE);
            $this->db->update('answers');
        }
        return true;
        }
        else{
            return false;
        }
      
        
    }
    
    public function getVote($answerid,$type){
        if($type == 'up'){
          return $this->db->query('select ratingup from answers where answerid='.$answerid.'')->row()->ratingup;
        }
        else{
            return $this->db->query('select ratingdown from answers where answerid='.$answerid.'')->row()->ratingdown;
        }
        
    }
    public function postQuestion($userId, $title, $cat, $question, $tags) {
        $data = array('title' => $title, 'question' => $question, 'userid' => $userId, 'catid' => $cat);
        $this->db->insert('questions', $data);

        $quesID = $this->db->query("select * from questions where userid = " . $userId . " and title = '" . $title . "'")->row()->questionid;
        $token = strtok($tags, ",");
        while ($token != false) {
            $check = $this->db->query("select * from tags where name = '" . $token . "'");
            if ($check->num_rows() >= 1) {
                $tagid = $check->row()->tagid;
                $qtag = array('questionid' => $quesID, 'tagid' => $tagid);
                $this->db->insert('qtags', $qtag);
            } else {
                $tag = array('name' => $token);
                $this->db->insert('tags', $tag);
                $tagid = $this->db->query("select * from tags where name = '" . $token . "'");
                $qtag = array('questionid' => $quesID, 'tagid' => $tagid->row()->tagid);
                $this->db->insert('qtags', $qtag);
            }
            $token = strtok(",");
        }
    }

    public function getQuestion($id) {
        $this->db->trans_start();
        $this->db->from('questions');
        $this->db->where('questionid', $id);
        $query['question'] = $this->db->get();

        $query['answers'] = $this->db->query('select * from answers a, user u where a.userid= u.userid and a.questionid=' . $id . '');
        $this->db->trans_complete();
        return $query;
    }
    
    public function postAnswerIn($quesID, $answer, $userid) {
                $enter = array('questionid' => $quesID,'answer' => $answer, 'userid' => $userid );
                $this->db->insert('answers', $enter);
                
    }
	
	public function getalltags(){
		
		$query = $this->db->get('tags');
        $rows_array = $query->result_array();
        return $rows_array;
	}

}

?> 