<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class User_model extends CI_Model {

	function __construct() {

		$this -> load -> database();

	}


	public function login($username, $password) {

		$this->db->where(array('username' => $username,'password' => sha1($password)));
                $res = $this->db->get('user',array('*'));
                if ($res->num_rows() != 1) { // should be only ONE matching row!!
                    return false;
                 }
                 // remember login
                   $session_id = $this->session->userdata('session_id');
                // remember current login
                $row = $res->row_array();
                $this->db->insert('logins',array('name' => $row['name'],'session_id' => $session_id));
                   return $res->row_array();
	}
        
        function is_loggedin() {
            $session_id = $this->session->userdata('session_id');
            $res = $this->db->get_where('logins',array('session_id' => $session_id));
            if ($res->num_rows() == 1) {
                $row = $res->row_array();
            return $row['name'];
             }
            else {
            return false;
            }
    }

	public function register($name,$email, $username, $password, $userType) {
                // is username unique?
        $res = $this->db->get_where('user',array('username' => $username));
        if ($res->num_rows() > 0) {
            return 'Username already exists';
        }
        else{
            $hashpwd = sha1($password);
        $data = array('name' => $name, 'email' => $email, 'username' => $username, 'password' => $hashpwd, 'type' => $userType);
        $this->db->insert('user',$data);
        return null; // no error message because all is ok
        }
        
	}

	public function getName($username, $password) {

		$query = $this -> db -> query("select * from user where username='" . $username . "' and password='" . $password . "'");

		$row = $query->row();
			$result['name'] = $row -> name;
			$result['id'] = $row -> userid;
                        $result['type'] = $row->type;
		
		return $result;
	}

	public function getDetails($id){
            return $this->db->query('select * from user where userid = '.$id.'');
        }
	
	public function getuserdetailsbyname($name){
		  $res = $this->db->get_where('user', array('name' => $name));
        if ($res->num_rows() == 1) {
            $row = $res->row_array();

            return $row;
        } else {
            return false;
        }
        /* $result->result_array();
            	
		$quary = "select * from user where username='" . $username . "'";
		$this->db->select('*')
		
		
		echo $quary;
            return $this->db->query($quary);
		 
		 */
       }
	
	public function getallusers(){
		
		$query = $this->db->get('user');
        $rows_array = $query->result_array();
        return $rows_array;
	}
	

}
?>
