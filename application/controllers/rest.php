<?php

class Rest extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this -> load -> model('search_model');
		$this -> load -> model('user_model');
		$this -> load -> model('questions_model');
		

	}

	// we'll explain this in a couple of slides time
	public function _remap() {
		// first work out which request method is being used
		$request_method = $this -> input -> server('REQUEST_METHOD');
		switch (strtolower($request_method)) {
			case 'get' :
				$this -> getDataFunction();
				break;
			case 'post' :
				$this -> postDataFunction();
				break;

			default :
				show_error('Unsupported method', 404);
				// CI function for 404 errors
				break;
		}
	}

	public function getDataFunction() {
		// we assume the URL is constructed using name/value pairs
		$args = $this -> uri -> uri_to_assoc(2);
		switch ($args['resource']) {

			case 'search_questions' :
				$res = $this -> search_model -> getQuestions($args);
				if ($res === false) {
					show_error('Unsupported request', 404);
				} else {
					// assume we get back an array of data - now echo it as JSON

					//return json_encode($res);

					echo json_encode($res);
				}
				break;

			case 'get_userdetails' :
				$res = $this -> user_model -> is_loggedin();

				if ($res === false) {

					//show_error('Unsupported request', 404);

					$this -> load -> view('view_loginpage');

				} else {
					// assume we get back an array of data - now echo it as JSON
					//echo json_encode($res);
					//alert("get_userdetails else incoming");

					$details = $this -> user_model -> getuserdetailsbyname($res);

					echo json_encode($details);

				}

				break;

			case 'getallusers' :
				$res = $this -> user_model -> is_loggedin();

				if ($res === false) {

					$this -> load -> view('view_loginpage');
				} else {

					$details = $this -> user_model -> getallusers();

				echo json_encode($details);

				}

				break;
				
				case 'getalltags' :
				$res = $this -> user_model -> is_loggedin();

				if ($res === false) {

					$this -> load -> view('view_loginpage');
				} else {

					$details = $this -> questions_model -> getalltags();

				echo json_encode($details);

				}

				break;

			default :
				show_error('Unsupported resource', 404);
				break;
		}
	}

	public function postDataFunction() {
		// we assume the URL is constructed using name/value pairs
		$args = $this -> uri -> uri_to_assoc(2);
		
		switch ($args['resource']) {

			case 'edit_userdetails' :
				$new_password = $this->input->post('password');
				
				$res = $this -> user_model -> is_loggedin();

				if ($res === false) {

					$this -> load -> view('view_loginpage');
				} else {

					$details = $this -> user_model -> getuserdetailsbyname($res);

					//echo json_encode($details);

				}

				break;

			default :
				show_error('Unsupported resource', 404);
				break;
		}
	}

}
?>
