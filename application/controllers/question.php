<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Question extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
                $this->load->model('questions_model');
	}
	public function index(){
            
	}
        
        public function getQuestion(){
            $id = $this -> uri -> segment(3);
            $res = $this -> questions_model -> getQuestion($id);
            
            $data['ques'] = $res;
            $this->load->view('question',$data);
        }
        
        public function postAnswer() {
        $answer = $_POST['writeAnswer'];
        $questionid = $this->uri->segment(3);
        
        if($answer == ''){
            show_error('Unsupported method', 404);
        }
        else{
            $this->questions_model->postAnswerIn($questionid,$answer,$this->session->userdata('userID'));
            redirect('/question/getquestion/'.$questionid.'');
        }
    }
    
    public function vote(){
        if($this->session->userdata('loggedIn') == 'True'){
            $answerid = $_POST['id'];
        $type = $_POST['voteType'];
        
       if($this->questions_model->addVote($answerid,$type,$this->session->userdata('userID'))){
          echo $this->questions_model->getVote($answerid,$type); 
       }
       else{
           echo '<script>alert("Already Voted!")</script>';
           
       }
        
        }
        else{
           echo '<script>alert("Login required to vote!")</script>';
            
        }
        
        
    }
	
}
