<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Profile extends CI_Controller{
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
	}
	
	public function index(){
		if($this->session->userdata('loggedIn')=== 'True'){
                   $this->load->view('profile_view');  
                }
                else{
                   $data['errmsg'] = 'Please login to view Profile';        
                    $this->load->view('view_loginpage', $data);
                }
                   
               
	}
	
}
