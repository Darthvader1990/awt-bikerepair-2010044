<?php

class Auth extends CI_Controller {

    function __construct() {
	parent::__construct();
	$this->load->library('authlib');
	$this->load->helper('url');
    }

    public function index() {
	redirect('/auth/login'); // url helper function
    }
    
    public function homepage() {
            $sesdata = array('loggedIn' => 'False');
		$this -> session -> set_userdata($sesdata);
            
		$this -> load -> view('view_homepage');
	}
        

    public function login() {	
	$data['errmsg'] = '';        
	$this->load->view('view_loginpage', $data);
    }

    public function register() {
	$this->load->view('view_registration', array('errmsg' => ''));
    }

    public function createaccount() {
                $name = $_POST['name'];
		$email = $_POST['email'];
		$username = $_POST['username'];
		$password = $_POST['password'];
		$userType = $_POST['userType'];
                $conf_password = $_POST['repassword'];		
	
	

	if (!($errmsg = $this->authlib->register($name, $email, $username, $password, $conf_password,$userType))) {
	    redirect('/auth/login');
	} else {
	    $data['errmsg'] = $errmsg;
	    $this->load->view('view_registration', $data);
	}
    }

    public function authenticate() {
	$username = $_POST['username'];
	$password = $_POST['password'];
	$user = $this->authlib->login($username, $password);
	if ($user !== false) {
            $sesdata = array('name' => $user['name'], 'userID' => $user['userid'], 'loggedIn' => 'True', 'type' => $user['type']);
            $this -> session -> set_userdata($sesdata);
	    $this->load->view('view_homepage');
            
	} else {
	    $data['errmsg'] = 'Invalid username or password';
	    $this->load->view('view_loginpage', $data);
	}
    }
    public function logout() {
	    $this -> session -> unset_userdata('name');
		$this -> session -> unset_userdata('userID');
		$this -> session -> unset_userdata('loggedIn');
		$this -> session -> unset_userdata('type');
		$this -> session -> sess_destroy();
		//redirect('site/homepage');
		$this -> index();
    }
    
    
        
        public function postQuestion(){
            $title = $_POST['titleText'];
            $category = $_POST['categorySet'];
            $question = $_POST['questionBoxText'];
            $tags = $_POST['tagsText'];
            
            $this->load->model('questions_model');
            $this->questions_model->postQuestion($this->session->userdata('userID'),$title,$category,$question,$tags);
            $this->load->view('view_homepage');
            
        }
    
    public function changepassview() {
	    $data['errmsg'] = '';
	    $this->load->view('changepass_view', $data);
    }
    public function changepass() {
	$paraUsername = $this->input->post('username');
	$paraOldPass = $this->input->post('oldpass');
	$paraNewPass = $this->input->post('newpass');
	if (!($errmsg = $this->authlib->changePassword($paraUsername,$paraOldPass,$paraNewPass))) {
	    $data['errmsg'] = 'pASSSWORD cHANGED';
	     $this->load->view('changepass_view', $data);
	} else {
	    $data['errmsg'] = $errmsg;
	    $this->load->view('changepass_view', $data);
	}
    }       
}

?>