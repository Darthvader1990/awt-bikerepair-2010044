<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Site extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this -> load -> helper('url');

		//$this->load->library('tank_auth');
	}

	public function index() {
            $this->load->library('authlib');
    $loggedin = $this->authlib->is_loggedin();
 
    if ($loggedin === false) {
        $this->load->helper('url');
        redirect('/auth/homepage');
 
    }
    else{
      $this->load->view('view_homepage');  
    }
    
            
            /*
		$sesdata = array('loggedIn' => 'False');
		$this -> session -> set_userdata($sesdata);
		$this -> homepage();
*/
	}
	
	public function askQuestion() {
            $this-> load -> model('questions_model');
                $data['cat'] = $this-> questions_model->getCat();
		$this -> load -> view('ask_question',$data);
	}
	
	public function getQuestions() {
		$this -> load -> model('user_model');
		$ques = $this -> user_model -> getAllQues();

		$this -> load -> view('questions', $ques);
	}

        public function postQuestion(){
            $title = $_POST['titleText'];
            $category = $_POST['categorySet'];
            $question = $_POST['questionBoxText'];
            $tags = $_POST['tagsText'];
            
            $this->load->model('questions_model');
            $this->questions_model->postQuestion($this->session->userdata('userID'),$title,$category,$question,$tags);
            $this->homepage();
            
        }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
