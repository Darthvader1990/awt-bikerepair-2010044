<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Questions extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
	}
	
	public function index(){
            $this->load->model('questions_model');
            $data['ques'] = $this->questions_model->getAllQues();
            
            $this->load->view('questions',$data);
		
	}
     /*   
        function questions_get()  
    {  
        $data = array('returned: '. $this->get('id'));  
        $this->response($data);  
    }
    */
         
	
}
